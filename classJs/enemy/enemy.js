export class Enemy {
    name;
    type;
    health = Math.floor(Math.random() * (70 - 4)) + 4; //Attribue une valeur random min 4 max 70
    hitStrength = Math.floor(Math.random() * (12 - 2)) + 2; //Attribue une valeur random min 2 max 12
    lvl = Math.floor(Math.random() * (10 - 1)) + 1; //Attribue une valeur random min 1 max 10
    xp = 0;
    greeting;
    img;
    tempHealth;
    dieSays = "oh no ! :'( ";
    winSays = "Haha ! You had no chance !";

    constructor(name = "Enemy") {
        this.name = name;
        this.tempHealth = this.health;
    }
    getName() {
        return this.name;
    }
    setName(newName) {
        this.name = newName;
    }

    getHealth() {
        return this.health;
    }
    setHealth(newHealth) {
        //modifier la valeur actuelle de health (combat)
        this.health = newHealth;
    }
    getTempHealth() {
        return this.tempHealth;
    }
    setTempHealth(newTempHealth) {
        //modifier la valeur actuelle de health (combat)
        this.tempHealth = newTempHealth;
    }
    getHitStrength() {
        return this.hitStrength;
    }
    setHitStrength(newHitStrenght) {
        //modifier la valeur actuelle de hitStrength (combat)
        this.hitStrength = newHitStrenght;
    }
    getLvl() {
        return this.lvl;
    }
    setLvl(newLvl) {
        //modifier la valeur actuelle de Lvl (combat)
        this.lvl = newLvl;
    }
    getXp() {
        return this.xp;
    }
    setXp(newXp) {
        //modifier la valeur actuelle de xp (combat)
        this.xp = newXp;
    }
    getType() {
        return this.type;
    }
    setType(newType) {
        //modifier la valeur actuelle de type
        this.type = newType;
    }
    attack(cible) {
        return cible.tempHealth -= this.hitStrength;
    }

    isDead() {
        if (this.tempHealth <= 0) {
            return true;
        } else {
            return false;
        }
    }

    levelUp() {
        if (this.xp == 10) {
            this.xp -= 10;
            this.lvl++;
            //A chaque niveau, gagne +10% health et hitStrength
            this.health += (this.health * 10) / 100;
            this.hitStrength += (this.hitStrength * 10) / 100;
        }
    }

    afficherEnemy() {
        const enemyTitle = document.getElementById("enemyTitle");
        const enemyLvl = document.getElementById("enemyLvl");
        const enemyHealth = document.getElementById("enemyHealth");
        const enemyHitStrenght = document.getElementById("enemyHitStrenght");
        const enemyGreeting = document.getElementById("enemyGreeting");
        const enemyImg = document.getElementById("enemyImg");


        enemyTitle.innerHTML = `${this.name}, ${this.type}`;
        enemyLvl.innerHTML = `Level : ${this.lvl}`;
        enemyHealth.innerHTML = `Health : ${this.health}`;
        enemyHitStrenght.innerHTML = `Hit Strenght : ${this.hitStrength}`;
        enemyGreeting.innerHTML = `${this.greeting}`;

        enemyImg.src = this.img;

    }

}