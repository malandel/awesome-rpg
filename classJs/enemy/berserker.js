import { Enemy } from './enemy.js'

export class Berserker extends Enemy {
    type = "berserker";
    greeting = "I'll feast on your blood !";
    img = "https://cdna.artstation.com/p/assets/images/images/010/165/202/large/dominik-kasprzycki-berserkers-art.jpg?1522919176";

    getAttacked(opponent) {
        //Resistance de 30%
        return Math.floor(opponent.hitStrength -= (opponent.hitStrength * 30 / 100));
    }

}