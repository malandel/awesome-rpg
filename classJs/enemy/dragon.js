import { Enemy } from './enemy.js';

export class Dragon extends Enemy {
    type = 'dragon';
    greeting = "Fear me, mortal !";
    status = "ground";
    img = "https://i.pinimg.com/originals/49/25/fc/4925fcc9668a85113da99ec1f861fa83.jpg";

    fly(cible) {
        //Resistance de 10%
        return Math.floor(cible.hitStrength -= 1.1 * cible.hitStrength);
    }

    attackFromSky(cible) {
        //Attaque + 10%
        return Math.floor(cible.tempHealth -= (this.hitStrength + (this.hitStrength * 1, 1)));
    }
    groundAttack(cible) {
        //Attaque au sol (on annule le bonus de attackFromSky)
        return Math.floor(cible.tempHealth -= (this.hitStrength - (this.hitStrength / 1.1)));
    }
    attack(cible) {
        //Surcharge de la méthode attack
        if (this.status == "ground") {
            this.groundAttack(cible);
            this.status == "fly";
        } else if (this.status == "fly") {
            this.fly(cible);
            this.status == "attackFromSky"
        } else if (this.status == "attackFromSky") {
            this.attackFromSky(cible);
            this.status == "ground";
        }
    }
    getAttacked(opponent) {
        //Resistance de 50%
        return opponent.hitStrength -= 1.5 * opponent.hitStrength;
    }
}