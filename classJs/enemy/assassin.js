import { Enemy } from './enemy.js';

export class Assassin extends Enemy {
    type = 'assassin';
    greeting = "I am your shadow";
    img = "https://i.pinimg.com/originals/7b/f9/81/7bf981f750ce9797baf06efc4f68aa60.jpg";

    attack(cible) {
        //attaque + 10%
        return cible.tempHealth -= Math.round(this.hitStrength += (this.hitStrength * 10) / 100);
    }
}