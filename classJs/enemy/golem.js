import { Enemy } from './enemy.js'

export class Golem extends Enemy {
    type = "golem";
    greeting = "I'll burn you !";
    img = "https://i.pinimg.com/originals/c2/a1/a0/c2a1a0c731aaed0eda86ade4ade4d576.jpg";

    getAttacked(opponent) {
        //Génère un nombre flottant entre 0 et 1
        let chance = Math.random();
        //Exécute le code dans 50% des cas
        if (chance <= 0.5) {
            //Réduit les dommages de l'ennemi de 100%
            return opponent.hitStrength = 0;
        }
    }
}