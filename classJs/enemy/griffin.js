import { Enemy } from './enemy.js';

export class Griffin extends Enemy {
    type = 'griffin';
    status = "ground";
    greeting = "I'm watching you !";
    img = "https://i.pinimg.com/736x/ec/c2/95/ecc295e15a9cc76bb1a94a5166af094c.jpg";

    fly(cible) {
        //Resistance de 10%
        return Math.floor(cible.hitStrength -= 0.1 * cible.hitStrength);
    }

    attackFromSky(cible) {
        //Attaque + 10%
        return Math.floor(cible.tempHealth -= (this.hitStrength + (this.hitStrength * 0, 1)));
    }
    groundAttack(cible) {
        //Attaque au sol
        return Math.floor(cible.tempHealth -= this.hitStrength);
    }
    attack(cible) {
        //Surcharge de la méthode attack
        if (this.status == "ground") {
            this.groundAttack(cible);
            this.status = "fly";
        } else if (this.status == "fly") {
            this.fly(cible);
            this.status = "attackFromSky"
        } else {
            this.attackFromSky(cible);
            this.status = "ground";
        }
    }
}