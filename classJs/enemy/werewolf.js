import { Enemy } from './enemy.js';

export class Werewolf extends Enemy {
    type = 'werewolf';
    greeting = "Watch the moon !";
    img = "https://i.pinimg.com/originals/c3/a0/67/c3a067485229d05a119d8b5c5630a337.jpg";

    getAttacked(opponent) {
        //Resistance de 50%
        return Math.floor(opponent.hitStrength -= 1.5 * opponent.hitStrength);
    }
}