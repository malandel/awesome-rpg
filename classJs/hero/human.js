import { Hero } from './hero.js';

export class Human extends Hero {
    race = "human";
    greeting = "Let's fight, peasant ! ";
    img = 'https://mir-s3-cdn-cf.behance.net/project_modules/disp/35632a15313421.562935084072e.jpg';

    attack(opponent) {
        if (opponent.status == "attackFromSky") {
            //Applique le bonus de classe -10% hitStrength
            return opponent.tempHealth -= Math.floor(this.hitStrength -= (this.hitStrength * 10 / 100));
        }
        //Applique le malus de classe +10% hitStrength
        return opponent.tempHealth -= Math.floor(this.hitStrength += (this.hitStrength * 10 / 100));

    }

};