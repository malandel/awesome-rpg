export class Hero {
    name;
    race;
    health = Math.floor(Math.random() * (70 - 4)) + 4; //Attribue une valeur random min 4 max 70
    hitStrength = Math.floor(Math.random() * (12 - 2)) + 2; //Attribue une valeur random min 2 max 12
    lvl = Math.floor(Math.random() * (10 - 1)) + 1; //Attribue une valeur random min 1 max 10
    xp = 0;
    greeting;
    img;
    tempHealth;
    dieSays = "oh no ! :'( ";
    winSays = "Haha ! You had no chance !";

    constructor(name = "Hero") {
        this.name = name;
        this.tempHealth = this.health;
    }

    getName() {
        return this.name;
    }
    setName(newName) {
        this.name = newName;
    }

    getHealth() {
        return this.health;
    }
    setHealth(newHealth) {
        //modifier la valeur actuelle de health (combat)
        this.health = newHealth;
    }
    getHitStrength() {
        return this.hitStrength;
    }
    setHitStrength(newHitStrenght) {
        //modifier la valeur actuelle de hitStrength (combat)
        this.hitStrength = newHitStrenght;
    }
    getLvl() {
        return this.lvl;
    }
    setLvl(newLvl) {
        //modifier la valeur actuelle de Lvl (combat)
        this.lvl = newLvl;
    }
    getXp() {
        return this.xp;
    }
    setXp(newXp) {
        //modifier la valeur actuelle de xp (combat)
        this.xp += newXp;
    }
    getRace() {
        return this.race;
    }
    setRace(newRace) {
        //modifier la valeur actuelle de race
        this.race = newRace;
    }
    getTempHealth() {
        return this.tempHealth;
    }
    setTempHealth(newTempHealth) {
        //modifier la valeur actuelle de health (combat)
        this.tempHealth = newTempHealth;
    }
    attack(cible) {
        return cible.tempHealth -= this.hitStrength;
    }

    isDead() {
        if (this.tempHealth <= 0) {
            return true;
        } else {
            return false;
        }
    }

    levelUp() {
        if (this.xp === 10) {
            this.xp -= 10;
            this.lvl++;
            //A chaque niveau, gagne +10% health et hitStrength
            this.health += this.health * 1.1;
            this.hitStrength += this.hitStrength * 1.1;
        }
    }
    afficherHero() {
        const heroTitle = document.getElementById("heroTitle");
        const heroLvl = document.getElementById("heroLvl");
        const heroHealth = document.getElementById("heroHealth");
        const heroHitStrenght = document.getElementById("heroHitStrenght");
        const heroGreeting = document.getElementById("heroGreeting");
        const heroImg = document.getElementById("heroImg");


        heroTitle.innerHTML = `${this.name}, ${this.race}`;
        heroLvl.innerHTML = `Level : ${this.lvl}`;
        heroHealth.innerHTML = `Health : ${this.health}`;
        heroHitStrenght.innerHTML = `Hit Strenght : ${this.hitStrength}`;
        heroGreeting.innerHTML = `${this.greeting}`;

        heroImg.src = this.img;

    }
}