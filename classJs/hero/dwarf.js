import { Hero } from './hero.js';

export class Dwarf extends Hero {
    race = "dwarf";
    greeting = "BEER !";
    img = "https://i.pinimg.com/originals/87/34/de/8734de74850688c5c4af8c8a9bace951.jpg"


    getAttacked(opponent) {
        //Génère un nombre flottant entre 0 et 1
        let chance = Math.random();
        //Exécute le code dans 20% des cas
        if (chance <= 0.2) {
            //Réduit les dommages de l'ennemi de 50%
            return Math.floor(opponent.hitStrength / 2);
        }
    }
}