import { Hero } from './hero.js';

export class Elf extends Hero {
    race = "elf";
    greeting = "Do you have any lembas left for me ? ";
    img = "https://cdnb.artstation.com/p/assets/images/images/012/004/043/large/olie-boldador-elfcompilesignedred.jpg?1532528328";


    attack(opponent) {
        if (opponent.status == "attackFromSky") {
            //Applique le bonus de classe +10% hitStrength
            return opponent.tempHealth -= Math.floor(this.hitStrength += (this.hitStrength + (this.hitStrength * 10 / 100)));
        }
        //Applique le malus de classe -10% hitStrength
        return opponent.tempHealth -= Math.floor(this.hitStrength -= (this.hitStrength - (this.hitStrength * 10 / 100)));

    }

};