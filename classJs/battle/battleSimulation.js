import { Hero } from "../hero/hero.js";
import { Enemy } from "../enemy/enemy.js";
import { Battle } from "../battle/battle.js";
import { Assassin } from "../enemy/assassin.js";

export class BattleSimulation {
    title; //string
    time;
    winner; //object
    loser;
    numberOfFights; //int
    fighter1; //object
    fighter2;
    opponents = [];
    battleHero; //boolean

    constructor(fighter1) {
        this.fighter1 = fighter1;
    }

    getTitle() {
        return this.title;
    }

    setTitle(fighter1) {
        this.title = (this.battleHero == true ? `${fighter1.name} vs. Monsters` : 'Monsters Tournament');
    }

    getFighter1() {
        return this.fighter1;
    }

    setFighter1(newFighter1) {
        this.fighter1 = newFighter1;
    }
    getFighter2() {
        return this.fighter2.name;
    }

    setFighter2(newFighter2) {
        this.fighter2 = newFighter2;
    }
    getOpponents() {
        this.opponents.forEach(enemy => {
            return enemy;
        });
    }
    setOpponents(newOpponents) {
        this.Opponents = newOpponents;
    }
    getWinner() {
        return this.winner;
    }
    setWinner(newWinner) {
        this.winner = newWinner;
    }
    getNumberOfFights() {
        return this.numberOfFights;
    }
    setNumberOfFights(newNumber) {
        this.numberOfFights = newNumber;
    }
    setWinner(newWinner) {
        this.winner = newWinner;
    }
    getTime() {
        return this.time;
    }
    setTime(newTime) {
        this.time = newTime;
    }
    getLoser() {
        return this.loser;
    }
    setLoser(newLoser) {
        this.loser = newLoser;
    }
    list(){
        this.opponents.push({newFighter2});
        this.fighter2 += newFighter2;

    }

    fight(premier, second) {
        //Reset compteurs
        this.setWinner("");
        this.setLoser("");
        this.setTime(0);
        this.fighter2 = new Assassin('Rog');
        //Détermine qui commence
        if (Math.floor(Math.random()) % 2 == 0) {
            premier = this.fighter1;
            second = this.fighter2;
        } else {
            premier = this.fighter2;
            second = this.fighter1;
        }
        console.log("Premier : ");
        console.log(premier);
        console.log("Second : ");
        console.log(second);

        for (let i = 1; i > 0; i++) {
            //Compte le nombre de tours
            this.setTime(i);
            console.log("Time :");
            console.log(this.time);
            //Attack
            premier.attack(second);
            console.log("premier attack");
            console.log(premier.attack(second));

            //Vérifie si l'opposant est mort
            if (second.isDead()) {
                console.log("second est mort");
                console.log(second.isDead());
                //Si oui, premier devient gagnant et deuxième perdant
                this.setWinner(premier);
                this.setLoser(second);
                console.log(this.winner);
                this.endFight(this.winner, this.loser);
                //On stoppe la boucle
                return;
            } else {
                //Sinon initie un nouveau tour
                this.setTime(i += 1);
                console.log("Time :");
                console.log(this.time);

                //C'est maintenant le deuxième qui attaque
                second.attack(premier);
                console.log("second attack");
                console.log(second.attack(premier));

                //Teste si l'adversaire est mort
                if (premier.isDead()) {
                    console.log("premier isdead");
                    console.log(premier.isDead());

                    this.setWinner(second);
                    this.setLoser(premier);
                    console.log(this.winner);
                    this.endFight(this.winner, this.loser);
                    return;
                }
                //Sinon on refait un tour de boucle
            }

        }
    }
    endFight(winner, loser) {
        //Winner gagne + 2 xp
        winner.setXp(2);
        //Winner gagne +10% de la vie de l'ennemi (heal)
        let newTempHealth = winner.tempHealth + (loser.health * 0.1);
        winner.setTempHealth(newTempHealth);

    }

    tournamentHero(fighter1){
       this.setNumberOfFights(1);
       console.log('Combat numéro :');
       console.log(this.numberOfFights);
       this.fight();
         while(!fighter1.isDead()){
            console.log('Tu continues');
            this.numberOfFights += 1;
            console.log('Combat numéro :');
            console.log(this.numberOfFights);
            this.fight();
        }
        console.log('Dead');
    }
}