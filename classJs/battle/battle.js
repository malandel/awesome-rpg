import { Enemy } from "../enemy/enemy.js";
import { Hero } from "../hero/hero.js";

export class Battle {
    title;
    winner;
    loser;
    time;
    fighter1;
    fighter2;
    premier;
    second;
    constructor(fighter1, fighter2) {
        this.fighter1 = fighter1;
        this.fighter2 = fighter2;
        this.setTitle(this.fighter1, this.fighter2);
    }
    getTitle() {
        return this.title;
    }
    setTitle(fighter1, fighter2) {
        this.title = `${fighter1.name} vs. ${fighter2.name}`;
    }

    getFighter1() {
        return this.fighter1;
    }
    setFighter1(newFighter1) {
        this.fighter1 = newFighter1;
    }
    getFighter2() {
        return this.fighter2;
    }
    setFigther2(newFighter2) {
        this.fighter2 = newFighter2;
    }
    getWinner() {
        return this.winner;
    }
    setWinner(newWinner) {
        this.winner = newWinner;
    }
    getLoser() {
        return this.loser;
    }
    setLoser(newLoser) {
        this.loser = newLoser;
    }
    getPremier() {
        return this.premier;
    }
    setPremier(newPremier) {
        this.premier = newPremier;
    }
    getSecond() {
        return this.second;
    }
    setSecond(newSecond) {
        this.second = newSecond;
    }
    getTime() {
        return this.time;
    }
    setTime(newTime) {
        this.time = newTime;
    }

    fight(fighter1, fighter2) {

        //Reset compteurs
        this.setWinner("");
        this.setLoser("");
        this.setTime(0);
        //Heal from the previous fight
        this.fighter1.setTempHealth(this.fighter1.health);
        this.fighter2.setTempHealth(this.fighter2.health);

        //Génère un chiffre random entre 1 et 10
        let j = Math.floor(Math.random() * (10 - 1) + 1);

        //Détermine qui commence
        if (j % 2 === 0) {
            this.premier = fighter1;
            this.second = fighter2;
        } else {
            this.premier = fighter2;
            this.second = fighter1;
        }

        console.log(this.premier.name)


        for (let i = 1; i > 0; i++) {
            //Compte le nombre de tours
            this.setTime(i);
            //Séparateur pour lisibilité (bricolage, technique pas propre)
            this.afficherFight(`-----------------------`);
            //On affiche le tour
            this.afficherFight(`Round : ${this.time} `);


            //Attack
            this.premier.attack(this.second);
            this.afficherFight(`${this.premier.name} attacks ${this.second.name}`);
            this.afficherFight(`${this.second.name}'s health is now ${this.second.tempHealth}`);


            //Vérifie si l'opposant est mort
            if (this.second.isDead()) {

                this.afficherFight(`${this.second.name} is dead.`);
                this.afficherFight(`-----------------------`);
                //Si oui, premier devient gagnant et deuxième perdant
                this.setWinner(this.premier);
                this.setLoser(this.second);
                //On applique au gagnant le +2xp et +10% de vie du perdant
                this.endFight(this.winner, this.loser);
                //Afficher les paroles des personnages
                this.afficherFight(`${this.loser.name} says ${this.loser.dieSays}`);
                this.afficherFight(`${this.winner.name} says ${this.winner.winSays}`);
                //Afficher la vie du gagnant
                this.afficherFight(`${this.winner.name}'s health is now ${this.winner.health}`);
                this.afficherFight(`-----------------------`);

                //On stoppe la boucle
                return;
            } else {
                //Sinon initie un nouveau tour
                this.setTime(i += 1);
                this.afficherFight(`-----------------------`);
                this.afficherFight(`Round : ${this.time}`);


                //C'est maintenant le deuxième qui attaque
                this.second.attack(this.premier);
                this.afficherFight(`${this.second.name} attacks ${this.premier.name}`);
                this.afficherFight(`${this.premier.name}'s health is now ${this.premier.tempHealth}`);


                //Teste si l'adversaire est mort
                if (this.premier.isDead()) {

                    this.afficherFight(`${this.premier.name} is dead.`);
                    this.afficherFight(`-----------------------`);
                    //Détermine le gagnant et le perdant
                    this.setWinner(this.second);
                    this.setLoser(this.premier);
                    //On ajoute au gagnant 2xp et +10% de la vie du perdant
                    this.endFight(this.winner, this.loser)
                        //On affiche les phrases des personnages
                    this.afficherFight(`${this.loser.name} says : ${this.loser.dieSays}`);
                    this.afficherFight(`${this.winner.name} says : ${this.winner.winSays}`);
                    //Afficher la vie du gagnant
                    this.afficherFight(`${this.winner.name}'s health is now ${this.winner.health}`);
                    this.afficherFight(`-----------------------`);
                    return;
                }
                //Sinon on refait un tour de boucle
            }

        }

    }
    endFight(winner, loser) {
        //Winner gagne + 2 xp
        winner.setXp(2);
        //Winner gagne +10% de la vie de l'ennemi (heal)
        let newTempHealth = winner.tempHealth + (loser.health * 1.1);
        winner.setTempHealth(newTempHealth);
        winner.levelUp();
    }
    afficherFight(content) {
        let battleTour = document.getElementById("battleTour");
        let p = document.createElement("p");
        let myContent = document.createTextNode(content);
        p.appendChild(myContent);
        battleTour.appendChild(p);
    }
}