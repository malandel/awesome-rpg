# awesome-rpg

## Online project
- https://awesome-rpg.surge.sh/ 

## Kanban Board :
- https://trello.com/b/MxaJVkwr/awesome-rpg

## Project

Pour les besoins d'un studio de développement de jeux videos, vous etes chargés de concevoir et coder la partie logique d'un RPG.

### Le jeu comportera ces éléments :
- Des classes Heros : humans, elf, dwarf
- Des classes pour les ennemis: Dragon, Golem, Assassin, Griffin, Berserker, Werewolf
- Une class Battle permettant à un Hero de combattre un ennemi
- Une classe BattleSimulation permettant de faire combattre le Hero contre chaque ennemi jusqu'à la mort du Hero

Pour plus de détails se référer au diagramme de classe dans le dossier UML

# Dev informations

- If you want to run the project from your PC, run it from a local server, don't open it directly with your navigator or be prepared to face conflicts !
