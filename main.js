import { Hero } from './classJs/hero/hero.js'
import { Human } from './classJs/hero/human.js'
import { Dwarf } from './classJs/hero/dwarf.js'
import { Elf } from './classJs/hero/elf.js'
import { Enemy } from './classJs/enemy/enemy.js'
import { Assassin } from './classJs/enemy/assassin.js'
import { Berserker } from './classJs/enemy/berserker.js'
import { Golem } from './classJs/enemy/golem.js'
import { Griffin } from './classJs/enemy/griffin.js'
import { Dragon } from './classJs/enemy/dragon.js'
import { Werewolf } from './classJs/enemy/werewolf.js'
import { Battle } from './classJs/battle/battle.js'
import { BattleSimulation } from './classJs/battle/battleSimulation.js'

//GET ALL HTML ELEMENTS
const showHero = document.getElementById('showHero');
const formEnemy = document.getElementById('formEnemy');
const showEnemy = document.getElementById('showEnemy');
const initBattleHtml = document.getElementById('initBattle');
const showBattle = document.getElementById('showBattle');
const battleTitle = document.getElementById('battleTitle');
const heroSays = document.getElementById('heroSays');
const enemySays = document.getElementById('enemySays');
const initiative = document.getElementById('initiative');
const endFight = document.getElementById('endFight');
const showEndStats = document.getElementById('showEndStats');
const battleTour = document.getElementById('battleTour');

//CREATE NEW HERO
let hero;
const creerHero = () => {
    const heroName = document.getElementById('inputHeroName').value;
    const heroRace = document.getElementById('chooseRace').value;

    //Cacher la feuille de perso du héros précédent et le formulaire de création Enemy
    showHero.style.display = "none";
    formEnemy.style.display = "none";

    //Vérifier que les input soient bien remplis
    if (!heroName || !heroRace) {
        return alert("Please choose a race AND a name for your Hero");
    }

    //Créer un nouvel objet Hero selon la race choisie et avec le nom saisi par l'utilisateur
    switch (heroRace) {
        case 'human':
            hero = new Human(heroName);
            break;
        case 'elf':
            hero = new Elf(heroName);
            break;
        case 'dwarf':
            hero = new Dwarf(heroName);
            break;
    }

    //Afficher les stats du Hero
    hero.afficherHero();

    //Afficher la carte du personnage créé
    showHero.style.display = "block";

    //Afficher formulaire création Enemy
    formEnemy.style.display = "block";

};

let enemy;
//CREER NOUVEL ENNEMI
const creerEnemy = () => {
    const enemyName = document.getElementById('inputEnemyName').value;
    const enemyType = document.getElementById('chooseType').value;
    //Cacher la feuille de perso de l'ennemi précédent  et le bouton battle
    showEnemy.style.display = "none";
    initBattleHtml.style.display = "none";
    showBattle.style.display = "none";

    //Vérifier que les input soient bien remplis
    if (!enemyName || !enemyType) {
        return alert("Please choose a type AND a name for your Opponent");
    }

    //Créer un nouvel objet Enemy selon le type choisi et avec le nom saisi par l'utilisateur
    switch (enemyType) {
        case 'assassin':
            enemy = new Assassin(enemyName);
            break;
        case 'berserker':
            enemy = new Berserker(enemyName);
            break;
        case 'dragon':
            enemy = new Dragon(enemyName);
            break;
        case 'golem':
            enemy = new Golem(enemyName);
            break;
        case 'griffin':
            enemy = new Griffin(enemyName);
            break;
        case 'werewolf':
            enemy = new Werewolf(enemyName);
            break;
    }

    //Afficher les stats de l'Enemy
    enemy.afficherEnemy();

    //Afficher la carte du personnage créé
    showEnemy.style.display = "block";

    //Afficher formulaire de battle
    initBattleHtml.style.display = "block";
};
let battle;
const initBattle = () => {

    battle = new Battle(hero, enemy);
    return battle;
}

const afficherBattle = () => {
    //Vide le contenu de la div BattleTour avant chaque nouveau combat sans recharger la page
    battleTour.innerHTML = "";
    //Créer un nouveau combat
    initBattle();
    battle.fight(hero, enemy);


    showBattle.style.display = "block";
    battleTitle.innerHTML = battle.title;
    heroSays.innerHTML = `<strong>${battle.fighter1.name}</strong> says : <em>${hero.greeting}</em>`;
    enemySays.innerHTML = `<strong>${battle.fighter2.name}</strong> says : <em>${enemy.greeting}</em>`;
    initiative.innerHTML = `<strong>${battle.premier.name}</strong> begins.`;
    endFight.innerHTML = `<strong>${battle.winner.name}</strong> wins +2 xp and +10% health`;
    showEndStats.innerHTML = `<strong>${battle.winner.name}</strong> is now level ${battle.winner.lvl}, has ${battle.winner.health} health and ${battle.winner.xp} XP`;

}


//CREER HERO
//Bouton validation création héros
const createHero = document.getElementById('createHero');
//Quand le bouton "create my hero" est cliqué : appelle la fonction creerHero
createHero.addEventListener('click', creerHero);

//CREER ENEMY
//Bouton validation création enemy
const createEnemy = document.getElementById('createEnemy');
//Quand le bouton "create my Enemy" est cliqué : appelle la fonction creerEnemy
createEnemy.addEventListener('click', creerEnemy);

//BATTLE ENEMY VS HERO
//Bouton validation "now fight""
const initBattleButton = document.getElementById('initBattle');
//Quand le bouton "Now fight" est cliqué : appelle la fonction afficherBattle
initBattleButton.addEventListener('click', afficherBattle);